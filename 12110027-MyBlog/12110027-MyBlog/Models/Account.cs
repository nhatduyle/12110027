﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace _12110027_MyBlog.Models
{
    public class Account
    {
        public int AccountID { get; set; }
        [Required(ErrorMessage = "Trường bắt buộc nhập")]
        [DataType(DataType.Password)]
        public string Password { get; set; }
        [Required(ErrorMessage = "Trường bắt buộc nhập")]
        [EmailAddress(ErrorMessage = "Email không hợp lệ")]
        public string Email { get; set; }
        [Required(ErrorMessage = "Trường bắt buộc nhập")]
        [StringLength(100, ErrorMessage = "Tối đa 100 ký tự")]
        public string FirstName { get; set; }
        [Required(ErrorMessage = "Trường bắt buộc nhập")]
        [StringLength(100, ErrorMessage = "Tối đa 100 ký tự")]

        public string LastName { get; set; }

        public virtual ICollection<Post> Posts { get; set; }
    }
}