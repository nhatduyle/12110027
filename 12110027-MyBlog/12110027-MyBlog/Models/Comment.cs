﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace _12110027_MyBlog.Models
{
    public class Comment
    {
        public int ID { get; set; }
        [Required(ErrorMessage = "Trường bắt buộc nhập")]
        [StringLength(10000, MinimumLength = 50, ErrorMessage = "Tối thiểu 50 ký tự")]
        public string Body { get; set; }
        [Required(ErrorMessage = "Trường bắt buộc nhập")]
        [DataType(DataType.DateTime)]
        public DateTime DateCreated { get; set; }
        [Required(ErrorMessage = "Trường bắt buộc nhập")]
        [DataType(DataType.DateTime)]
        public DateTime DateUpdated { get; set; }
        [Required(ErrorMessage = "Trường bắt buộc nhập")]
        public string Author { get; set; }
        [Required(ErrorMessage = "Trường bắt buộc nhập")]
        public int PostID { get; set; }
        public virtual Post Post { get; set; }
    }
}