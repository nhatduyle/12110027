﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace _12110027_MyBlog.Models
{
    [Table("BaiViet")]
    public class Post
    {
        public int PostID { get; set; }
        [Required(ErrorMessage = "Trường bắt buộc nhập")]
        [StringLength(500, MinimumLength = 20, ErrorMessage = "Từ 20 đến 500 ký tự")]

        public string Title { get; set; }
        [Required(ErrorMessage = "Trường bắt buộc nhập")]
        [StringLength(10000, MinimumLength = 50, ErrorMessage = "Tối thiểu 50 ký tự")]
        public string Body { get; set; }
        [Required(ErrorMessage = "Trường bắt buộc nhập")]
        [DataType(DataType.DateTime)]
        public DateTime DateCreated { get; set; }
        [Required(ErrorMessage = "Trường bắt buộc nhập")]
        [DataType(DataType.DateTime, ErrorMessage = "Ngày không hợp lệ")]
        public DateTime DateUpdated { get; set; }
        [Required(ErrorMessage = "Trường bắt buộc nhập")]
        public int AccountID { get; set; }

        public virtual ICollection<Comment> Comments { get; set; }
        public virtual ICollection<Tag> Tags { get; set; }
        public virtual Account Account { get; set; }
    }
}