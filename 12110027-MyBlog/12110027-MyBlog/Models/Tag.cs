﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace _12110027_MyBlog.Models
{
    public class Tag
    {
        public int TagID { get; set; }
        [Required(ErrorMessage = "Trường bắt buộc nhập")]
        [StringLength(100, MinimumLength = 10, ErrorMessage = "Từ 10 đến 100 ký tự")]
        public string Content { get; set; }

        public virtual ICollection<Post> Post { get; set; }
    }
}