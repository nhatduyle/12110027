﻿using System.Web;
using System.Web.Mvc;

namespace MyWeb_12110027
{
    public class FilterConfig
    {
        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            filters.Add(new HandleErrorAttribute());
        }
    }
}