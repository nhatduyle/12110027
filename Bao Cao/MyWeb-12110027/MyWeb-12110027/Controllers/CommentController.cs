﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using MyWeb_12110027.Models;

namespace MyWeb_12110027.Controllers
{
    public class CommentController : Controller
    {
        const int heSoDiemSoBinhLuan = 5;
        public int LayUserDangNhap()
        {
            int iduser = db.UserProfiles.Select(x => new { x.UserId, x.UserName })
                        .Where(y => y.UserName == User.Identity.Name).Single().UserId;
            return iduser;
        }
       
        public void TinhDiemLai()
        {
            int idUser = this.LayUserDangNhap();
            Profile pro = db.Profiles.Where(o => o.UserProfile_UserId == idUser).Single();
            int soBinhLuan = db.Comments.Count(t => t.Profile_UserId == idUser);
            pro.DiemConLai = pro.DiemTichLuy - pro.DiemSuDung;
            pro.DiemTichLuy = pro.SoBaiViet * heSoDiemSoBinhLuan + pro.SoBinhLuan;
            pro.SoBinhLuan = soBinhLuan;
            db.SaveChanges();
        }
        private WebDbContext db = new WebDbContext();

        //
        // GET: /Comment/

        public ActionResult Index()
        {
            var comments = db.Comments.Include(c => c.Post).Include(c => c.Profile);
            return View(comments.ToList());
        }

        //
        // GET: /Comment/Details/5

        public ActionResult Details(int id = 0)
        {
            Comment comment = db.Comments.Find(id);
            if (comment == null)
            {
                return HttpNotFound();
            }
            return View(comment);
        }

        //
        // GET: /Comment/Create

        public ActionResult Create()
        {
            ViewBag.PostID = new SelectList(db.Posts, "PostID", "TieuDe");
            ViewBag.Profile_UserId = new SelectList(db.Profiles, "UserProfile_UserId", "HoTen");
            return View();
        }

        //
        // POST: /Comment/Create

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(Comment comment, int idpost, int iduser)
        {
            if (ModelState.IsValid)
            {
                comment.NgayCapNhat = DateTime.Now;
                comment.NgayTao = DateTime.Now;
                comment.PostID = idpost;
                if (Request.IsAuthenticated)
                {
                    comment.Profile_UserId = iduser;
                }
                else
                {
                    comment.Profile_UserId = 4;
                }
                db.Comments.Add(comment);
                ViewBag.HoTen = db.Profiles.Where(p => p.UserProfile_UserId == iduser).Select(t=>t.HoTen).FirstOrDefault();
                db.SaveChanges();
                if(comment.Profile_UserId!=4)
                    TinhDiemLai();
                return PartialView("viewComment", comment);
            }

            ViewBag.PostID = new SelectList(db.Posts, "PostID", "TieuDe", comment.PostID);
            ViewBag.Profile_UserId = new SelectList(db.Profiles, "UserProfile_UserId", "HoTen", comment.Profile_UserId);
            return View(comment);
        }

        //
        // GET: /Comment/Edit/5

        public ActionResult Edit(int id = 0)
        {
            Comment comment = db.Comments.Find(id);
            if (comment == null)
            {
                return HttpNotFound();
            }
            ViewBag.PostID = new SelectList(db.Posts, "PostID", "TieuDe", comment.PostID);
            ViewBag.Profile_UserId = new SelectList(db.Profiles, "UserProfile_UserId", "HoTen", comment.Profile_UserId);
            return View(comment);
        }

        //
        // POST: /Comment/Edit/5

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(Comment comment)
        {
            if (ModelState.IsValid)
            {
                db.Entry(comment).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.PostID = new SelectList(db.Posts, "PostID", "TieuDe", comment.PostID);
            ViewBag.Profile_UserId = new SelectList(db.Profiles, "UserProfile_UserId", "HoTen", comment.Profile_UserId);
            return View(comment);
        }

        //
        // GET: /Comment/Delete/5

        public ActionResult Delete(int id = 0)
        {
            Comment comment = db.Comments.Find(id);
            if (comment == null)
            {
                return HttpNotFound();
            }
            return View(comment);
        }

        //
        // POST: /Comment/Delete/5

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Comment comment = db.Comments.Find(id);
            db.Comments.Remove(comment);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}