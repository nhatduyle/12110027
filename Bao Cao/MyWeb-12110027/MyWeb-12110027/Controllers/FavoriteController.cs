﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using MyWeb_12110027.Models;

namespace MyWeb_12110027.Controllers
{
    public class FavoriteController : Controller
    {
        private WebDbContext db = new WebDbContext();

        //
        // GET: /Favorite/

        public ActionResult Index()
        {
            var favorites = db.Favorites.Include(f => f.Post).Include(f => f.Profile);
            return View(favorites.ToList());
        }

        //
        // GET: /Favorite/Details/5

        public ActionResult Details(int id = 0)
        {
            Favorite favorite = db.Favorites.Find(id);
            if (favorite == null)
            {
                return HttpNotFound();
            }
            return View(favorite);
        }

        //
        // GET: /Favorite/Create

        public ActionResult Create()
        {
            ViewBag.PostID = new SelectList(db.Posts, "PostID", "TieuDe");
            ViewBag.Profile_UserId = new SelectList(db.Profiles, "UserProfile_UserId", "HoTen");
            return View();
        }

        //
        // POST: /Favorite/Create

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(Favorite favorite)
        {
            if (ModelState.IsValid)
            {
                db.Favorites.Add(favorite);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.PostID = new SelectList(db.Posts, "PostID", "TieuDe", favorite.PostID);
            ViewBag.Profile_UserId = new SelectList(db.Profiles, "UserProfile_UserId", "HoTen", favorite.Profile_UserId);
            return View(favorite);
        }

        //
        // GET: /Favorite/Edit/5

        public ActionResult Edit(int id = 0)
        {
            Favorite favorite = db.Favorites.Find(id);
            if (favorite == null)
            {
                return HttpNotFound();
            }
            ViewBag.PostID = new SelectList(db.Posts, "PostID", "TieuDe", favorite.PostID);
            ViewBag.Profile_UserId = new SelectList(db.Profiles, "UserProfile_UserId", "HoTen", favorite.Profile_UserId);
            return View(favorite);
        }

        //
        // POST: /Favorite/Edit/5

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(Favorite favorite)
        {
            if (ModelState.IsValid)
            {
                db.Entry(favorite).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.PostID = new SelectList(db.Posts, "PostID", "TieuDe", favorite.PostID);
            ViewBag.Profile_UserId = new SelectList(db.Profiles, "UserProfile_UserId", "HoTen", favorite.Profile_UserId);
            return View(favorite);
        }

        //
        // GET: /Favorite/Delete/5

        public ActionResult Delete(int id = 0)
        {
            Favorite favorite = db.Favorites.Find(id);
            if (favorite == null)
            {
                return HttpNotFound();
            }
            return View(favorite);
        }

        //
        // POST: /Favorite/Delete/5

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Favorite favorite = db.Favorites.Find(id);
            db.Favorites.Remove(favorite);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}