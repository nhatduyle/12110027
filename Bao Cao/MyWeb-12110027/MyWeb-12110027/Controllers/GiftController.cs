﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using MyWeb_12110027.Models;

namespace MyWeb_12110027.Controllers
{
    public class GiftController : Controller
    {
        private WebDbContext db = new WebDbContext();

        //
        // GET: /Gift/

        public ActionResult Index()
        {
            var gifts = db.Gifts.Include(g => g.Profile).Include(g => g.Voucher);
            return View(gifts.ToList());
        }

        //
        // GET: /Gift/Details/5

        public ActionResult Details(int id = 0)
        {
            Gift gift = db.Gifts.Find(id);
            if (gift == null)
            {
                return HttpNotFound();
            }
            return View(gift);
        }

        //
        // GET: /Gift/Create

        public ActionResult Create()
        {
            ViewBag.Profile_UserId = new SelectList(db.Profiles, "UserProfile_UserId", "HoTen");
            ViewBag.VoucherID = new SelectList(db.Vouchers, "VoucherID", "Name");
            return View();
        }

        //
        // POST: /Gift/Create

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(Gift gift)
        {
            if (ModelState.IsValid)
            {
                db.Gifts.Add(gift);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.Profile_UserId = new SelectList(db.Profiles, "UserProfile_UserId", "HoTen", gift.Profile_UserId);
            ViewBag.VoucherID = new SelectList(db.Vouchers, "VoucherID", "Name", gift.VoucherID);
            return View(gift);
        }

        //
        // GET: /Gift/Edit/5

        public ActionResult Edit(int id = 0)
        {
            Gift gift = db.Gifts.Find(id);
            if (gift == null)
            {
                return HttpNotFound();
            }
            ViewBag.Profile_UserId = new SelectList(db.Profiles, "UserProfile_UserId", "HoTen", gift.Profile_UserId);
            ViewBag.VoucherID = new SelectList(db.Vouchers, "VoucherID", "Name", gift.VoucherID);
            return View(gift);
        }

        //
        // POST: /Gift/Edit/5

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(Gift gift)
        {
            if (ModelState.IsValid)
            {
                db.Entry(gift).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.Profile_UserId = new SelectList(db.Profiles, "UserProfile_UserId", "HoTen", gift.Profile_UserId);
            ViewBag.VoucherID = new SelectList(db.Vouchers, "VoucherID", "Name", gift.VoucherID);
            return View(gift);
        }

        //
        // GET: /Gift/Delete/5

        public ActionResult Delete(int id = 0)
        {
            Gift gift = db.Gifts.Find(id);
            if (gift == null)
            {
                return HttpNotFound();
            }
            return View(gift);
        }

        //
        // POST: /Gift/Delete/5

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Gift gift = db.Gifts.Find(id);
            db.Gifts.Remove(gift);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}