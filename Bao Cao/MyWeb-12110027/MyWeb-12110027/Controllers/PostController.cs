﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using MyWeb_12110027.Models;

namespace MyWeb_12110027.Controllers
{
    public class PostController : Controller
    {
        private WebDbContext db = new WebDbContext();

        //
        // GET: /Post/

        const int heSoDiemSoBinhLuan = 5;
        public int LayUserDangNhap()
        {
            int iduser = db.UserProfiles.Select(x => new { x.UserId, x.UserName })
                        .Where(y => y.UserName == User.Identity.Name).Single().UserId;
            return iduser;
        }
        public void TinhDiemLai()
        {
            int idUser = this.LayUserDangNhap();
            Profile pro = db.Profiles.Where(o => o.UserProfile_UserId == idUser).Single();
            int soBaiViet = db.Posts.Count(t => t.Profile_UserId == idUser);
            pro.DiemConLai = pro.DiemTichLuy - pro.DiemSuDung;
            pro.DiemTichLuy = pro.SoBaiViet * heSoDiemSoBinhLuan + pro.SoBinhLuan;
            pro.SoBaiViet = soBaiViet;
            db.SaveChanges();
        }

        public ActionResult Index()
        {
            var posts = db.Posts.Include(p => p.Profile).Include(p => p.Category);
            return View(posts.ToList());
        }
        public ActionResult IndexThucDon()
        {
            var posts = db.Posts.Include(p => p.Profile).Include(p => p.Category).Where(i => i.CategoryID == 1);
            return View(posts.ToList());
        }
        public ActionResult IndexPhuongPhap()
        {
            var posts = db.Posts.Include(p => p.Profile).Include(p => p.Category).Where(i => i.CategoryID == 2);
            return View(posts.ToList());
        }
        public ActionResult IndexDienDan()
        {
            var posts = db.Posts.Include(p => p.Profile).Include(p => p.Category).Where(i => i.CategoryID == 4);
            return View(posts.ToList());
        }
        //
        // GET: /Post/Details/5

        public ActionResult Details(int id = 0)
        {
            int user = 4;
            Post post = db.Posts.Find(id);
            ViewBag.Rating = post.RatingPost;
            ViewData["idpost"] = id;
            if (Request.IsAuthenticated)
            {
                user = LayUserDangNhap();
                ViewData["iduser"] = user;
            }
            else
                ViewData["iduser"] = 4;
            if (user != 4) //không phải là KHÁCH
            {
                string chuoiRating = "Đánh giá của bạn: ";
                if (db.Ratings.Count(p => p.PostID == id && p.Profile_UserId == user) > 0)
                {
                    int ra = db.Ratings.Where(p => p.PostID == id && p.Profile_UserId == user).Select(r => r.RatingValue).Single();
                    if (ra == 1)
                        chuoiRating += "<b><a href=/Post/Rating?post=" + id + "&value=1&ratingnew=0> 1 </a></b>";
                    else
                        chuoiRating += "<a href=/Post/Rating?post=" + id + "&value=1&ratingnew=0> 1 </a>";
                    if (ra == 2)
                        chuoiRating += "<b><a href=/Post/Rating?post=" + id + "&value=2&ratingnew=0> 2 </a></b>";
                    else
                        chuoiRating += "<a href=/Post/Rating?post=" + id + "&value=2&ratingnew=0> 2 </a>";
                    if (ra == 3)
                        chuoiRating += "<b><a href=/Post/Rating?post=" + id + "&value=3&ratingnew=0> 3 </a></b>";
                    else
                        chuoiRating += "<a href=/Post/Rating?post=" + id + "&value=3&ratingnew=0> 3 </a>";
                    if (ra == 4)
                        chuoiRating += "<b><a href=/Post/Rating?post=" + id + "&value=4&ratingnew=0> 4 </a></b>";
                    else
                        chuoiRating += "<a href=/Post/Rating?post=" + id + "&value=4&ratingnew=0> 4 </a>";
                    if (ra == 5)
                        chuoiRating += "<b><a href=/Post/Rating?post=" + id + "&value=5&ratingnew=0> 5 </a></b>";
                    else
                        chuoiRating += "<a href=/Post/Rating?post=" + id + "&value=5&ratingnew=0> 5 </a>";
                }
                else
                {
                    chuoiRating += "<a href=/Post/Rating?post=" + id + "&value=1&ratingnew=1> 1 </a>";
                    chuoiRating += "<a href=/Post/Rating?post=" + id + "&value=2&ratingnew=1> 2 </a>";
                    chuoiRating += "<a href=/Post/Rating?post=" + id + "&value=3&ratingnew=1> 3 </a>";
                    chuoiRating += "<a href=/Post/Rating?post=" + id + "&value=4&ratingnew=1> 4 </a>";
                    chuoiRating += "<a href=/Post/Rating?post=" + id + "&value=5&ratingnew=1> 5 </a>";
                }
                ViewBag.YourRating = chuoiRating +" sao";

                string chuoi = "";
                if (db.Favorites.Count(p => p.PostID == id && p.Profile_UserId == user) > 0)
                    chuoi = "<a href=/Post/Favorite/" + id + "?huy=1>Hủy bỏ yêu thích</a>";
                else
                    chuoi = "<a href=/Post/Favorite/" + id + "?&huy=0>Thêm vào yêu thích</a>";
                ViewBag.YeuThich = chuoi;
            }
            else
            {
                ViewBag.YeuThich = "Vui lòng đăng nhập để ghi lại yêu thích của bạn";
                ViewBag.YourRating = "Vui lòng đăng nhập để đánh giá bài viết";
            }

            if (post == null)
            {
                return HttpNotFound();
            }
            return View(post);

        }

        //
        // GET: /Post/Create

        public ActionResult Create()
        {
            ViewBag.Profile_UserId = new SelectList(db.Profiles, "UserProfile_UserId", "HoTen");
            ViewBag.CategoryID = new SelectList(db.Categories, "CategoryID", "Name");
            return View();
        }

        //
        // POST: /Post/Create

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(Post post, string loai)
        {
            if (ModelState.IsValid)
            {
                if (loai == "ThucDon")
                    post.CategoryID = 1;
                else
                    if (loai == "PhuongPhap")
                        post.CategoryID = 2;
                    else
                        if (loai == "DienDan")
                            post.CategoryID = 4;
                //loai==Video????
                post.NgayCapNhat = DateTime.Now;
                post.NgayTao = DateTime.Now;
                post.Profile_UserId = LayUserDangNhap();
                post.TongRating = 0;
                post.TongUserRating = 0;
                db.Posts.Add(post);
                db.SaveChanges();

                if (post.Profile_UserId != 4)
                    TinhDiemLai();

                if(loai=="ThucDon")
                    return RedirectToAction("IndexThucDon");
                if (loai == "PhuongPhap")
                    return RedirectToAction("IndexPhuongPhap");
                if (loai == "DienDan")
                    return RedirectToAction("IndexDienDan");
            }

            ViewBag.Profile_UserId = new SelectList(db.Profiles, "UserProfile_UserId", "HoTen", post.Profile_UserId);
            ViewBag.CategoryID = new SelectList(db.Categories, "CategoryID", "Name", post.CategoryID);
            return View(post);
        }
        public ActionResult Favorite(Post post, int id, int huy)
        {
            Favorite fa = new Favorite();
            int user = LayUserDangNhap();
            if (huy == 0)
            {
                fa.PostID = id;
                fa.Profile_UserId = user;
                db.Favorites.Add(fa);
                db.SaveChanges();
            }
            else
            {
                fa = db.Favorites.Where(p => p.Profile_UserId == user && p.PostID == id).Single();
                db.Favorites.Remove(fa);
                db.SaveChanges();
            }
            return RedirectToAction("Details/" + id, "Post");
        }
        public ActionResult Rating(int post, int value, int ratingnew)
        {
            Rating ra = new Rating();
            Post po = new Post();
            po = db.Posts.Where(p => p.PostID == post).Single();
            int user = LayUserDangNhap();
            if (ratingnew == 1)
            {
                ra.PostID = post;
                ra.Profile_UserId = user;
                ra.RatingValue = value;
                db.Ratings.Add(ra);
                po.TongRating = po.TongRating + value;
                po.TongUserRating = po.TongUserRating + 1;
                db.SaveChanges();
            }
            else { 
                ra = db.Ratings.Where(p => p.PostID == post && p.Profile_UserId == user).Single();
                ra.RatingValue = value;
                po.TongRating = po.TongRating - db.Ratings.Where(p => p.PostID == post && p.Profile_UserId == user)
                    .Select(r => r.RatingValue).Single(); //giá trị cũ
                po.TongRating = po.TongRating + value; //giá trị mới
                db.SaveChanges();
            }
            return RedirectToAction("Details/" + post, "Post");
        }


        //
        // GET: /Post/Edit/5

        public ActionResult Edit(int id = 0)
        {
            Post post = db.Posts.Find(id);
            if (post == null)
            {
                return HttpNotFound();
            }
            ViewBag.Profile_UserId = new SelectList(db.Profiles, "UserProfile_UserId", "HoTen", post.Profile_UserId);
            ViewBag.CategoryID = new SelectList(db.Categories, "CategoryID", "Name", post.CategoryID);
            return View(post);
        }

        //
        // POST: /Post/Edit/5

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(Post post)
        {
            if (ModelState.IsValid)
            {
                post.NgayCapNhat = DateTime.Now;
                db.Entry(post).State = EntityState.Modified;
                db.SaveChanges();
                if (post.CategoryID == 1)
                    return RedirectToAction("IndexThucDon");
                else
                    if (post.CategoryID == 2)
                    return RedirectToAction("IndexPhuongPhap");
                    else
                        return RedirectToAction("IndexDienDan");
            }
            ViewBag.Profile_UserId = new SelectList(db.Profiles, "UserProfile_UserId", "HoTen", post.Profile_UserId);
            ViewBag.CategoryID = new SelectList(db.Categories, "CategoryID", "Name", post.CategoryID);
            return View(post);
        }

        //
        // GET: /Post/Delete/5

        public ActionResult Delete(int id = 0)
        {
            Post post = db.Posts.Find(id);
            if (post == null)
            {
                return HttpNotFound();
            }
            return View(post);
        }

        //
        // POST: /Post/Delete/5

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Post post = db.Posts.Find(id);
            db.Posts.Remove(post);
            db.SaveChanges();
            return RedirectToAction("BaiVietCuaBan", "Profile");
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}