﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using MyWeb_12110027.Models;

namespace MyWeb_12110027.Controllers
{
    public class ProfileController : Controller
    {
        private WebDbContext db = new WebDbContext();

        //
        // GET: /Profile/
        public ActionResult Index()
        {
            var profiles = db.Profiles.Include(p => p.UserProfile).Include(p => p.Level);
            return View(profiles.ToList());
        }
        public ActionResult IndexThucDon() //Yêu thích
        {
            int user = 4;
            if (Request.IsAuthenticated)
            {
                user = db.UserProfiles.Select(x => new { x.UserId, x.UserName })
                            .Where(y => y.UserName == User.Identity.Name).Single().UserId;
            }
            if (user == 4)
                return RedirectToAction("Register", "Account");
            var PostYeuThich = db.Posts.Where(p => p.Favorites.Any(i => i.Profile_UserId == user) && p.CategoryID == 1);
            return View(PostYeuThich.ToList());
        }
        public ActionResult IndexPhuongPhap() //Yêu thích
        {
            int user = db.UserProfiles.Select(x => new { x.UserId, x.UserName })
                        .Where(y => y.UserName == User.Identity.Name).Single().UserId;
            var PostYeuThich = db.Posts.Where(p => p.Favorites.Any(i => i.Profile_UserId == user) && p.CategoryID == 2);
            return View(PostYeuThich.ToList());
        }
        public ActionResult IndexDienDan() //Yêu thích
        {
            int user = db.UserProfiles.Select(x => new { x.UserId, x.UserName })
                        .Where(y => y.UserName == User.Identity.Name).Single().UserId;
            var PostYeuThich = db.Posts.Where(p => p.Favorites.Any(i => i.Profile_UserId == user) && p.CategoryID == 4);
            return View(PostYeuThich.ToList());
        }

        public ActionResult Favorite(int id, string loai)
        {
            // return RedirectToAction("Favorite", "Post", new { id = id, huy = 1 });

            Favorite fa = new Favorite();
            int user = db.UserProfiles.Select(x => new { x.UserId, x.UserName })
                        .Where(y => y.UserName == User.Identity.Name).Single().UserId;
            fa = db.Favorites.Where(p => p.Profile_UserId == user && p.PostID == id).Single();
            db.Favorites.Remove(fa);
            db.SaveChanges();
            if (loai == "ThucDon")
                return RedirectToAction("IndexThucDon", "Profile");
            else
                if (loai == "PhuongPhap")
                return RedirectToAction("IndexPhuongPhap", "Profile");
                else
                    return RedirectToAction("IndexDienDan", "Profile");
        }
        public ActionResult BaiVietCuaBan() //Bài viết của bạn
        {
            int user = db.UserProfiles.Select(x => new { x.UserId, x.UserName })
                        .Where(y => y.UserName == User.Identity.Name).Single().UserId;
            var YourPost = db.Posts.Where(p => p.Profile_UserId == user).OrderByDescending(t=>t.CategoryID);
            return View(YourPost.ToList());
        }

        //
        // GET: /Profile/Details/5

        public ActionResult Details()
        {
             int user = db.UserProfiles.Select(x => new { x.UserId, x.UserName })
                          .Where(y => y.UserName == User.Identity.Name).Single().UserId;
            Profile profile = db.Profiles.Where(p=>p.UserProfile_UserId==user).Single();
            if (profile == null)
            {
                return HttpNotFound();
            }
            return View(profile);
        }

        //
        // GET: /Profile/Create

        public ActionResult Create()
        {
            ViewBag.UserProfile_UserId = new SelectList(db.UserProfiles, "UserId", "UserName");
            ViewBag.LevelID = new SelectList(db.Levels, "LevelID", "TenCapBac");
            return View();
        }

        //
        // POST: /Profile/Create

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(Profile profile)
        {
            if (ModelState.IsValid)
            {
                db.Profiles.Add(profile);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.UserProfile_UserId = new SelectList(db.UserProfiles, "UserId", "UserName", profile.UserProfile_UserId);
            ViewBag.LevelID = new SelectList(db.Levels, "LevelID", "TenCapBac", profile.LevelID);
            return View(profile);
        }

        //
        // GET: /Profile/Edit/5

        public ActionResult Edit(int id = 0)
        {
            Profile profile = db.Profiles.Find(id);
            if (profile == null)
            {
                return HttpNotFound();
            }
            ViewBag.UserProfile_UserId = new SelectList(db.UserProfiles, "UserId", "UserName", profile.UserProfile_UserId);
            ViewBag.LevelID = new SelectList(db.Levels, "LevelID", "TenCapBac", profile.LevelID);
            return View(profile);
        }

        //
        // POST: /Profile/Edit/5

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(Profile profile)
        {
            if (ModelState.IsValid)
            {
                db.Entry(profile).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.UserProfile_UserId = new SelectList(db.UserProfiles, "UserId", "UserName", profile.UserProfile_UserId);
            ViewBag.LevelID = new SelectList(db.Levels, "LevelID", "TenCapBac", profile.LevelID);
            return View(profile);
        }

        //
        // GET: /Profile/Delete/5

        public ActionResult Delete(int id = 0)
        {
            Profile profile = db.Profiles.Find(id);
            if (profile == null)
            {
                return HttpNotFound();
            }
            return View(profile);
        }

        //
        // POST: /Profile/Delete/5

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Profile profile = db.Profiles.Find(id);
            db.Profiles.Remove(profile);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
        public ActionResult EditYourPost(int id)
        {
            return RedirectToAction("Edit/" + id, "Post");
        }
        public ActionResult DeleteYourPost(int id)
        {
            return RedirectToAction("Delete/" + id, "Post");
        }
        public ActionResult DetailsYourPost(int id)
        {
            return RedirectToAction("Details/" + id, "Post");
        }
    }
}