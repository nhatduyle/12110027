namespace MyWeb_12110027.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class lan1 : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Profiles",
                c => new
                    {
                        UserProfile_UserId = c.Int(nullable: false),
                        HoTen = c.String(),
                        GioiTinh = c.Boolean(nullable: false),
                        NgaySinh = c.DateTime(nullable: false),
                        Email = c.String(),
                        Password = c.String(),
                        NgayTaoTaiKhoan = c.DateTime(nullable: false),
                        DiaChi = c.String(),
                        NgheNghiep = c.String(),
                        DienThoai = c.String(),
                        DiemTichLuy = c.Int(nullable: false),
                        DiemSuDung = c.Int(nullable: false),
                        DiemConLai = c.Int(nullable: false),
                        SoBaiViet = c.Int(nullable: false),
                        SoBinhLuan = c.Int(nullable: false),
                        TinhTrang = c.Boolean(nullable: false),
                        LevelID = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.UserProfile_UserId)
                .ForeignKey("dbo.UserProfile", t => t.UserProfile_UserId)
                .ForeignKey("dbo.Levels", t => t.LevelID, cascadeDelete: true)
                .Index(t => t.UserProfile_UserId)
                .Index(t => t.LevelID);
            
            CreateTable(
                "dbo.UserProfile",
                c => new
                    {
                        UserId = c.Int(nullable: false, identity: true),
                        UserName = c.String(),
                    })
                .PrimaryKey(t => t.UserId);
            
            CreateTable(
                "dbo.Levels",
                c => new
                    {
                        LevelID = c.Int(nullable: false, identity: true),
                        TenCapBac = c.String(),
                        MoTa = c.String(),
                        Diem = c.Int(nullable: false),
                        HinhAnh = c.String(),
                    })
                .PrimaryKey(t => t.LevelID);
            
            CreateTable(
                "dbo.Comments",
                c => new
                    {
                        CommentID = c.Int(nullable: false, identity: true),
                        PostID = c.Int(nullable: false),
                        NoiDung = c.String(),
                        NgayTao = c.DateTime(nullable: false),
                        NgayCapNhat = c.DateTime(nullable: false),
                        Profile_UserId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.CommentID)
                .ForeignKey("dbo.Posts", t => t.PostID, cascadeDelete: true)
                .ForeignKey("dbo.Profiles", t => t.Profile_UserId, cascadeDelete: false)
                .Index(t => t.PostID)
                .Index(t => t.Profile_UserId);
            
            CreateTable(
                "dbo.Posts",
                c => new
                    {
                        PostID = c.Int(nullable: false, identity: true),
                        TieuDe = c.String(),
                        NoiDung = c.String(),
                        NgayTao = c.DateTime(nullable: false),
                        NgayCapNhat = c.DateTime(nullable: false),
                        TongRating = c.Int(nullable: false),
                        TongUserRating = c.Int(nullable: false),
                        Profile_UserId = c.Int(nullable: false),
                        CategoryID = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.PostID)
                .ForeignKey("dbo.Profiles", t => t.Profile_UserId, cascadeDelete: true)
                .ForeignKey("dbo.Categories", t => t.CategoryID, cascadeDelete: true)
                .Index(t => t.Profile_UserId)
                .Index(t => t.CategoryID);
            
            CreateTable(
                "dbo.Categories",
                c => new
                    {
                        CategoryID = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                    })
                .PrimaryKey(t => t.CategoryID);
            
            CreateTable(
                "dbo.Favorites",
                c => new
                    {
                        Profile_UserId = c.Int(nullable: false),
                        PostID = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.Profile_UserId, t.PostID })
                .ForeignKey("dbo.Posts", t => t.PostID, cascadeDelete: true)
                .ForeignKey("dbo.Profiles", t => t.Profile_UserId, cascadeDelete: false)
                .Index(t => t.PostID)
                .Index(t => t.Profile_UserId);
            
            CreateTable(
                "dbo.Ratings",
                c => new
                    {
                        PostID = c.Int(nullable: false),
                        Profile_UserId = c.Int(nullable: false),
                        RatingValue = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.PostID, t.Profile_UserId })
                .ForeignKey("dbo.Posts", t => t.PostID, cascadeDelete: true)
                .ForeignKey("dbo.Profiles", t => t.Profile_UserId, cascadeDelete: false)
                .Index(t => t.PostID)
                .Index(t => t.Profile_UserId);
            
            CreateTable(
                "dbo.Gifts",
                c => new
                    {
                        Profile_UserId = c.Int(nullable: false),
                        VoucherID = c.Int(nullable: false),
                        NgayDoi = c.DateTime(nullable: false),
                        TinhTrang = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => new { t.Profile_UserId, t.VoucherID })
                .ForeignKey("dbo.Profiles", t => t.Profile_UserId, cascadeDelete: true)
                .ForeignKey("dbo.Vouchers", t => t.VoucherID, cascadeDelete: true)
                .Index(t => t.Profile_UserId)
                .Index(t => t.VoucherID);
            
            CreateTable(
                "dbo.Vouchers",
                c => new
                    {
                        VoucherID = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                        NoiDung = c.String(),
                        SoLuong = c.Int(nullable: false),
                        DiemQuyDoi = c.Int(nullable: false),
                        HinhAnh = c.String(),
                    })
                .PrimaryKey(t => t.VoucherID);
            
        }
        
        public override void Down()
        {
            DropIndex("dbo.Gifts", new[] { "VoucherID" });
            DropIndex("dbo.Gifts", new[] { "Profile_UserId" });
            DropIndex("dbo.Ratings", new[] { "Profile_UserId" });
            DropIndex("dbo.Ratings", new[] { "PostID" });
            DropIndex("dbo.Favorites", new[] { "Profile_UserId" });
            DropIndex("dbo.Favorites", new[] { "PostID" });
            DropIndex("dbo.Posts", new[] { "CategoryID" });
            DropIndex("dbo.Posts", new[] { "Profile_UserId" });
            DropIndex("dbo.Comments", new[] { "Profile_UserId" });
            DropIndex("dbo.Comments", new[] { "PostID" });
            DropIndex("dbo.Profiles", new[] { "LevelID" });
            DropIndex("dbo.Profiles", new[] { "UserProfile_UserId" });
            DropForeignKey("dbo.Gifts", "VoucherID", "dbo.Vouchers");
            DropForeignKey("dbo.Gifts", "Profile_UserId", "dbo.Profiles");
            DropForeignKey("dbo.Ratings", "Profile_UserId", "dbo.Profiles");
            DropForeignKey("dbo.Ratings", "PostID", "dbo.Posts");
            DropForeignKey("dbo.Favorites", "Profile_UserId", "dbo.Profiles");
            DropForeignKey("dbo.Favorites", "PostID", "dbo.Posts");
            DropForeignKey("dbo.Posts", "CategoryID", "dbo.Categories");
            DropForeignKey("dbo.Posts", "Profile_UserId", "dbo.Profiles");
            DropForeignKey("dbo.Comments", "Profile_UserId", "dbo.Profiles");
            DropForeignKey("dbo.Comments", "PostID", "dbo.Posts");
            DropForeignKey("dbo.Profiles", "LevelID", "dbo.Levels");
            DropForeignKey("dbo.Profiles", "UserProfile_UserId", "dbo.UserProfile");
            DropTable("dbo.Vouchers");
            DropTable("dbo.Gifts");
            DropTable("dbo.Ratings");
            DropTable("dbo.Favorites");
            DropTable("dbo.Categories");
            DropTable("dbo.Posts");
            DropTable("dbo.Comments");
            DropTable("dbo.Levels");
            DropTable("dbo.UserProfile");
            DropTable("dbo.Profiles");
        }
    }
}
