﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MyWeb_12110027.Models
{
    public class Admin
    {
        public int ID { get; set; }
        public string GioiThieu { get; set; }
        public string LienKetFacebook { get; set; }
        public string LienKetTwitter { get; set; }
        public string LienKetGooglePlus { get; set; }
        public string LienKetYoutube { get; set; }
        public string HinhAnhCuaNgay { get; set; }

    }
}