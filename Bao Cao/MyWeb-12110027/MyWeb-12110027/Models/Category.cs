﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MyWeb_12110027.Models
{
    public class Category
    {
        public int CategoryID { get; set; }
        public string Name { get; set; }

        public virtual ICollection<Post> Posts { get; set; }
    }
}
