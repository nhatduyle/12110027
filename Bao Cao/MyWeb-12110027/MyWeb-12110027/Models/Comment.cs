﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;

namespace MyWeb_12110027.Models
{
    public class Comment
    {
        public int CommentID { get; set; }
        public int PostID { get; set; }
        public string NoiDung { get; set; }
        public DateTime NgayTao { get; set; }

        public int LastTime
        {
            get
            {
                return (DateTime.Now - NgayTao).Days * 60 * 24 + (DateTime.Now - NgayTao).Hours * 60 + (DateTime.Now - NgayTao).Minutes;
            }
        }
        public DateTime NgayCapNhat { get; set; }
        public virtual Post Post { get; set; }
        [ForeignKey("Profile")]
        public int Profile_UserId { get; set; }
        public virtual Profile Profile { get; set; }
    }
}
