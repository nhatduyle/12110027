﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;

namespace MyWeb_12110027.Models
{
    public class Gift
    {
        [Key]
        [Column(Order = 0)]
        [ForeignKey("Profile")]
        public int Profile_UserId { get; set; }
        [Key]
        [Column(Order = 1)]
        public int VoucherID { get; set; }
        public DateTime NgayDoi { get; set; }
        public bool TinhTrang { get; set; }

        public virtual Profile Profile { get; set; }
        public virtual Voucher Voucher { get; set; }
    }
}
