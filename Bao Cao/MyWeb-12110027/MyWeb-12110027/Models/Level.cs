﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MyWeb_12110027.Models
{
    public class Level
    {
        public int LevelID { get; set; }
        public string TenCapBac { get; set; }
        public string MoTa { get; set; }
        public int Diem { get; set; }
        public string HinhAnh { get; set; }

        public virtual ICollection<Profile> Profiles { get; set; }
    }
}
