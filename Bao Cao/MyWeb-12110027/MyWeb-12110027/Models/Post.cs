﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;

namespace MyWeb_12110027.Models
{
    public class Post
    {
       
        public int PostID { get; set; }
         [System.ComponentModel.DisplayName("Tiêu đề")]
        public string TieuDe { get; set; }
         [System.ComponentModel.DisplayName("Nội dung")]
        public string NoiDung { get; set; }
         [System.ComponentModel.DisplayName("Ngày tạo")]
        public DateTime NgayTao { get; set; }
         [System.ComponentModel.DisplayName("Ngày cập nhật")]
        public DateTime NgayCapNhat { get; set; }
        public int TongRating { get; set; }
        public int TongUserRating { get; set; }

         [System.ComponentModel.DisplayName("Rating")]
        public float RatingPost
        {
            get
            {
                if (TongUserRating != 0)
                    return TongRating / TongUserRating;
                else
                    return 0;
            }
        }

        [ForeignKey("Profile")]
        public int Profile_UserId { get; set; }
        public virtual Profile Profile { get; set; }
        public int CategoryID { get; set; }
        public virtual Category Category { get; set; }
        public virtual ICollection<Comment> Comments { get; set; }
        public virtual ICollection<Favorite> Favorites { get; set; }
        public virtual ICollection<Rating> Ratings { get; set; }
    }
}
