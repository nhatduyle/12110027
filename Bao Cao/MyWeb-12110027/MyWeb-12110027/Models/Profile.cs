﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace MyWeb_12110027.Models
{
    public class Profile
    {
        public string HoTen { get; set; }
        public bool GioiTinh { get; set; }
        public DateTime NgaySinh { get; set; }
        public string Email { get; set; }
        public string Password { get; set; }
        public DateTime NgayTaoTaiKhoan { get; set; }

        public string DiaChi { get; set; }
        public string NgheNghiep { get; set; }
        public string DienThoai { get; set; }
        public int DiemTichLuy { get; set; }
        public int DiemSuDung { get; set; }
        public int DiemConLai { get; set; }
        public int SoBaiViet { get; set; }
        public int SoBinhLuan { get; set; }

        public bool TinhTrang { get; set; } //Dang hoat dong/ Bi xoa
        [Key]
        [ForeignKey("UserProfile")]
        public int UserProfile_UserId { get; set; }
        public virtual UserProfile UserProfile { get; set; }
        public int LevelID { get; set; }
        public virtual Level Level { get; set; }

        public virtual ICollection<Comment> Comments { get; set; }
        public virtual ICollection<Post> Posts { get; set; }
        public virtual ICollection<Rating> Ratings { get; set; }
        public virtual ICollection<Favorite> Favorites { get; set; }
        public virtual ICollection<Gift> Gifts { get; set; }
    }
}