﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;

namespace MyWeb_12110027.Models
{
    public class Rating
    {
        [Key]
        [Column(Order = 0)]
        public int PostID { get; set; }
        [Key]
        [Column(Order = 1)]
        [ForeignKey("Profile")]
        public int Profile_UserId { get; set; }
        public int RatingValue { get; set; }

        public virtual Post Post { get; set; }
        public virtual Profile Profile { get; set; }
    }
}
