﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MyWeb_12110027.Models
{
    public class Voucher
    {
        public int VoucherID { get; set; }
        public string Name { get; set; }
        public string NoiDung { get; set; }
        public int SoLuong { get; set; }
        public int DiemQuyDoi { get; set; }
        public string HinhAnh { get; set; }

        public virtual ICollection<Gift> Gifts { get; set; }
    }
}
