﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace MyWeb_12110027.Models
{
    public class WebDbContext : DbContext
    {
        public WebDbContext()
            : base("DefaultConnection")
        {
        }
        public DbSet<Profile> Profiles { get; set; }
        public DbSet<UserProfile> UserProfiles { get; set; }
        public DbSet<Post> Posts { get; set; }
        public DbSet<Comment> Comments { get; set; }
        public DbSet<Rating> Ratings { get; set; }
        public DbSet<Voucher> Vouchers { get; set; }
        public DbSet<Favorite> Favorites { get; set; }
        public DbSet<Level> Levels { get; set; }
        public DbSet<Gift> Gifts { get; set; }
        public DbSet<Category> Categories { get; set; }
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
            modelBuilder.Entity<UserProfile>()
                .HasOptional(o => o.Profile)
                .WithRequired(r => r.UserProfile);
        }

        public DbSet<Admin> Admins { get; set; }
    }
}