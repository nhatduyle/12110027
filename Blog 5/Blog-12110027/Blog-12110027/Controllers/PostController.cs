﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Blog_12110027.Models;

namespace _12110027_Blog.Controllers
{
    [Authorize]
    public class PostController : Controller
    {
        private UsersContext db = new UsersContext();

        //
        // GET: /Post/
        [AllowAnonymous]
        public ActionResult Index()
        {    
           return View(db.Posts.ToList());
        }

        public ActionResult Search(string searchString)
        {
            var posts = from p in db.Posts select p;
            if (!string.IsNullOrEmpty(searchString))
            {
                posts = posts.Where(y => y.Tags.Any(t => t.Content.Contains(searchString)));
            }
            return View(posts);
        }

        
        //
        // GET: /Post/Details/5
        [AllowAnonymous]
        public ActionResult Details(int id = 0)
        {
            Post post = db.Posts.Find(id);
            ViewData["idpost"] = id;

            if (post == null)
            {
                return HttpNotFound();
            }
            return View(post);
        }

        //
        // GET: /Post/Create

        public ActionResult Create()
        {
            return View();
        }

        //
        // POST: /Post/Create

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(Post post, string Content)
        {
            if (ModelState.IsValid)
            {
                post.DateCreated = DateTime.Now;
                post.DateUpdated = DateTime.Now;
                int userid = db.UserProfiles.Select(x => new { x.UserId, x.UserName })
                    .Where(y => y.UserName == User.Identity.Name).Single().UserId;
                post.UserProfileID = userid;
                //Tạo list các tag
                List<Tag> Tags = new List<Tag>();
                //Tách các tag theo dấu ,
                string[] TagContent = Content.Split(',');
                //lập các tag vừa tách 
                foreach (string item in TagContent)
                {
                    //Tìm tag content đã có hay chưa
                    Tag tagExist = null;
                    var ListTag = db.Tags.Where(y => y.Content.Equals(item));
                    if (ListTag.Count() > 0)
                    {
                        //nếu có rồi thì add thêm post vào
                        tagExist = ListTag.First();
                        tagExist.Posts.Add(post);
                    }
                    else
                    {
                        //nếu chưa có tag thì tạo mới
                        tagExist = new Tag();
                        tagExist.Content = item;
                        tagExist.Posts = new List<Post>();
                        tagExist.Posts.Add(post);
                    }
                    //add vào list các tag
                    Tags.Add(tagExist);
                }
                //Gán list tag cho post
                post.Tags = Tags;
                db.Posts.Add(post);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(post);
        }

        //
        // GET: /Post/Edit/5

        public ActionResult Edit(int id = 0)
        {
            Post post = db.Posts.Find(id);
            int userid = db.UserProfiles.Select(x => new { x.UserId, x.UserName })
                    .Where(y => y.UserName == User.Identity.Name).Single().UserId;
            if (!post.UserProfileID.Equals(userid))
                return RedirectToAction("Login", "Account");
            post.DateUpdated = DateTime.Now;
            if (post == null)
            {
                return HttpNotFound();
            }
            return View(post);
        }

        //
        // POST: /Post/Edit/5

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(Post post)
        {
            if (ModelState.IsValid)
            {
                db.Entry(post).State = EntityState.Modified;
                post.DateUpdated = DateTime.Now;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(post);
        }

        //
        // GET: /Post/Delete/5

        public ActionResult Delete(int id = 0)
        {
            Post post = db.Posts.Find(id);
            int userid = db.UserProfiles.Select(x => new { x.UserId, x.UserName })
                    .Where(y => y.UserName == User.Identity.Name).Single().UserId;
            if (!post.UserProfileID.Equals(userid))
                return RedirectToAction("Login", "Account");
            if (post == null)
            {
                return HttpNotFound();
            }
            return View(post);
        }

        //
        // POST: /Post/Delete/5

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Post post = db.Posts.Find(id);
            db.Posts.Remove(post);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}