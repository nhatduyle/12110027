﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Blog_12110027.Models
{
    public class Comment
    {
        public int ID { get; set; }
        [Required(ErrorMessage = "Trường bắt buộc nhập")]
        public string Body { get; set; }
        [Required(ErrorMessage = "Trường bắt buộc nhập")]
        [DataType(DataType.DateTime)]
        public DateTime DateCreated { get; set; }
        [Required(ErrorMessage = "Trường bắt buộc nhập")]
        [DataType(DataType.DateTime)]
        public DateTime DateUpdated { get; set; }
        public int LastTime
        {
            get
            {
                return (DateTime.Now - DateCreated).Days * 60*24 + (DateTime.Now - DateCreated).Hours * 60 + (DateTime.Now - DateCreated).Minutes;
            }
        }
        [Required(ErrorMessage = "Trường bắt buộc nhập")]
        public string Author { get; set; }
        public int PostID { get; set; }
        public virtual Post Post { get; set; }
    }
}