﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Blog_12110027.Models
{
    public class Post
    {
        public int PostID { get; set; }
        [Required(ErrorMessage = "Trường bắt buộc nhập")]
        [StringLength(500, MinimumLength = 20, ErrorMessage = "Độ dài từ 20 đến 500 ký tự")]
        public string Title { get; set; }
        [Required(ErrorMessage = "Trường bắt buộc nhập")]
        [StringLength(1000000, MinimumLength = 50, ErrorMessage = "Tối thiểu 50 ký tự")]
        public string Body { get; set; }
        [Required(ErrorMessage = "Trường bắt buộc nhập")]
        [DataType(DataType.DateTime)]
        public DateTime DateCreated { get; set; }
        [Required(ErrorMessage = "Trường bắt buộc nhập")]
        [DataType(DataType.DateTime)]
        public DateTime DateUpdated { get; set; }

        public virtual ICollection<Comment> Comments { get; set; }
        public virtual ICollection<Tag> Tags { get; set; }
        public int UserProfileID { get; set; }
        public virtual UserProfile UserProfile { get; set; }
    }
}