﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Blog_12110027.Models
{
    public class Tag
    {
        public int TagID { get; set; }
        //     [StringLength(100, MinimumLength = 10, ErrorMessage = "Từ 10 đến 100 ký tự")]
        public string Content { get; set; }
        public virtual ICollection<Post> Posts { get; set; }
    }
}